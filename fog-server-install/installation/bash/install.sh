#!/bin/bash
#### Filename: install.sh
#### Creation Date: 2019-03-29T11:58:22+01:00
####
#### Last modified by:   Antonio Nardella
#### Last modified time: 2019-03-29T11:59:02+01:00
####
#### Copyright: 2019 Antonio Nardella - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later

#### Description: This scripts is the main installation script
#### for a proxmox template FOG server in a FUSS environment
#### Part 1: Ask for IP details and write them to file
#### Part 2: Ask for school name (FOG Location)
#### Part 3: Edit .fogsettings and upgrade FOG
#### Part 4: Edit the default FOG MySQL dump
#### Part 5: Import the edited FOG MySQL dump to the database
#### Part 6: Login to FUSS server and set parameters (PXE, TFTP)
#### Part XerX: Reboot the FOG server


# Check if running as root
if [[ "$EUID" = 0 ]]; then
    echo "Siamo già root"
else
    sudo -k # make sure to ask for password on next sudo
    if sudo true; then
        echo "Password inserita correttamente"
    else
        echo "Password sudo errata"
        exit 1
    fi
fi

# Part 1
. ./scripts/ethInterface.sh

# Part 2
. ./scripts/schoolLocation.sh

# Part 3
. ./scripts/upgradeFog.sh

# Part 4
. ./scripts/editDmp.sh

# Part 5
. ./scripts/importDmp.sh

# Part 6
. ./scripts/editFussServer.sh

echo "Indirizzo IP del server FOG configurato: ${ANSWERS[address]}"
echo "Nome della scuola configurato: ${ANSWERS[schoolName]}"
echo "Configurazione della banca dati completata"
