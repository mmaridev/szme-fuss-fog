#!/bin/bash
#### Filename: checkip.sh
#### Creation Date: 2019-03-26T17:41:34+01:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-04-05T17:33:31+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

#	Define Variables
# Expand the task list with words that describe the task
declare TASK_LIST="name email"
declare -A ANSWERS

#	Messages

	# Use the words from the task-list as string identifier
	MSG_STR_NAME="What is your name"
	MSG_STR_EMAIL="What is your email"
#
#	Action
#
	# Work the list
	for task in $TASK_LIST
	do
		# Prepare the message string
		str=MSG_STR_${task^^}
		# Loop while input is empty
		while [ -z "${ANSWERS[$task]}" ]
		do
			echo ${!str}
			read input
			ANSWERS[$task]=$input
		done
	done
	echo
	echo "Result values are:"
	for item in $TASK_LIST
	do
		echo "$item :: ${ANSWERS[$item]}"
	done
