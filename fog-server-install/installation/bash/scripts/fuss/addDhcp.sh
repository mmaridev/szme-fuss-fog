#!/bin/bash
#### Filename: addDhcp.sh
#### Creation Date: 2019-04-05T11:44:44+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-04-05T11:44:52+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later
#### Description: Add DHCP configuration for FOG server and PXE boot

# Define DHCP file on FUSS server
DHCP_ADDED="/etc/dhcp/dhcpd-added.conf"

cat << 'EOM' >$DHCP_ADDED
# Add local dhcpd configuration here.
# This file is loaded at the end of dhcpd.conf.

# This file is not touched by a fuss-server upgrade or similar command and
# it is installed empty.
  next-server fogserver;
  filename "undionly.kpxe";

 class "UEFI-32-1" {
    match if substring(option vendor-class-identifier, 0, 20) = "PXEClient:Arch:00006";
    filename "i386-efi/ipxe.efi";
    }

    class "UEFI-32-2" {
    match if substring(option vendor-class-identifier, 0, 20) = "PXEClient:Arch:00002";
     filename "i386-efi/ipxe.efi";
    }

    class "UEFI-64-1" {
    match if substring(option vendor-class-identifier, 0, 20) = "PXEClient:Arch:00007";
     filename "ipxe.efi";
    }

    class "UEFI-64-2" {
    match if substring(option vendor-class-identifier, 0, 20) = "PXEClient:Arch:00008";
    filename "ipxe.efi";
    }

    class "UEFI-64-3" {
    match if substring(option vendor-class-identifier, 0, 20) = "PXEClient:Arch:00009";
     filename "ipxe.efi";
    }

    class "Legacy" {
    match if substring(option vendor-class-identifier, 0, 20) = "PXEClient:Arch:00000";
    filename "undionly.kkpxe";
  }
EOM
