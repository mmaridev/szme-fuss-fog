#!/bin/bash
#### Filename: adaptTftp.sh
#### Creation Date: 2019-04-05T11:55:04+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-04-05T11:55:08+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later

# Define variables
DHCPD_CONF_FILE=/etc/dhcp/dhcpd.conf

# Comment "filename" and "next-server" to ensure that the FOG server is used for tftp
sed -i '/filename "/s/^/#/' $DHCPD_CONF_FILE
sed -i '/next-server 1/s/^/#/' $DHCPD_CONF_FILE


# Comment "group {}" block to ensure that the FOG server is used for tftp
sed -i '/^group {$/,/^}$/{s/^/#/}' $DHCPD_CONF_FILE
