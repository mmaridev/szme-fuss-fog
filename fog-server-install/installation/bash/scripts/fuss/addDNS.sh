#!/bin/bash
#### Filename: addDNS.sh
#### Creation Date: 2019-04-05T13:34:08+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-04-05T13:34:18+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later
#### Description: Adds FOG server as DNS CNAME

# Define variables
DB_LOCAL_FILE=/var/cache/bind/db.local

# Add fogserver as CNAME after proxy occurrence
echo "Add fogserver as CNAME after prxoy occurrence"
sed -i "/^proxy/a \$TTL 604800     ; 1 week\nfogserver               CNAME   $FS_HOST_NAME" $DB_LOCAL_FILE
sed -i "/^proxy/a \$TTL 3600       ; 1 hour\n$FS_HOST_NAME             A       ${ANSWERS[address]}" $DB_LOCAL_FILE

# Edit bind9 serial
echo "Edit bind9 serial"
sed -i "s/\s*[0-9]\+\s*;\s*serial/                                $DATE$FS_DNS_RND   ; serial/" $DB_LOCAL_FILE

# Reload bind9
echo "Reload bind9"
systemctl reload bind9
