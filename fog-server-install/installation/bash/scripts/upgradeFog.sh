#!/bin/bash
#### Filename: upgradeFogtest.sh
#### Creation Date: 2019-03-29T17:18:47+01:00
####
#### Last modified by:   Antonio Nardella
#### Last modified time: 2019-03-29T17:18:50+01:00
####
#### Copyright: 2019 Antonio Nardella - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later

# Define variables
FOG_SETTINGS_FILE=/opt/fog/.fogsettings

# edit /opt/fog/.fogsettings
# edit FOG server IP address
sed -i.bak 's/10.199.199.13/'"${ANSWERS[address]}"'/g' $FOG_SETTINGS_FILE

# edit submask
sed -i.bak 's/255.255.255.0/'"${ANSWERS[netmask]}"'/g' $FOG_SETTINGS_FILE

# edit dns server = FUSS server IP address
sed -i.bak 's/10.199.199.11/'"${ANSWERS[gateway]}"'/g' $FOG_SETTINGS_FILE

# update FOG
cd /home/root/Tools/fogproject-1.5.5
bash install.sh -y
