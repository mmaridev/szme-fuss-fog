#!/bin/bash
#### Filename: editDmp.sh
#### Creation Date: 2019-03-29T11:56:59+01:00
####
#### Last modified by:   Antonio Nardella
#### Last modified time: 2019-03-29T11:57:03+01:00
####
#### Copyright: 2019 Antonio Nardella - progetti@antonionardella.it / Guggi
####
#### SPDX-License-Identifier: GPL-3.0-or-later

# Define variables
FOG_DATABASE_FILE=/root/Tools/default.dmp

# Find lines and update FOG server IP address
sed -i.bak 's/10.199.199.13/'"${ANSWERS[address]}"'/g' $FOG_DATABASE_FILE

# Find line and update Location
sed -i.bak '/^INSERT INTO `location` VALUES/ s/FOGSUEDTIROL/'"${ANSWERS[schoolName]}"'/' $FOG_DATABASE_FILE
