#!/bin/bashaddDhcp.shaddDhcp.sh
#### Filename: editFussServertest.sh
#### Creation Date: 2019-03-29T13:51:49+01:00
####
#### Last modified by:   Antonio Nardella
#### Last modified time: 2019-03-29T13:51:52+01:00
####
#### Copyright: 2019 Antonio Nardella - progetti@antonionardella.it / Guggi / Marco Marinello
####
#### SPDX-License-Identifier: GPL-3.0-or-later

# Define variable
FS_HOST_NAME=$(hostname) # FOG server hostname used for SCRIPT3
FS_DNSDOMAINNAME=$(dnsdomainname) # FOG server dnsdomainname used for SCRIPT3
FS_DNS_DATE=$(date +%Y%m%d) # FOG server date used for SCRIPT3 as bind9 serial
FS_DNS_RND=$(($RANDOM % 100)) # FOG server random number between 0 and 99 used for SCRIPT3 as bind serial

# Define scripts
## SCRIPT1 add dhcpd-added.conf
echo "SCRIPT1"
SCRIPT1=./scripts/fuss/addDhcp.sh

## SCRIPT2 configure TFTP settings
echo "SCRIPT2"
SCRIPT2=./scripts/fuss/adaptTftp.sh

## SCRIPT3 add DNS names
echo "SCRIPT3"
SCRIPT3=./scripts/fuss/addDNS.sh

## Connect to FussServer
(
  cat $SCRIPT1 $SCRIPT2 $SCRIPT3
  echo "echo ${ANSWERS[address]}:fog server >> /etc/fuss-server/firewall-allowed-lan-hosts"
) | ssh -t root@${ANSWERS[gateway]}
