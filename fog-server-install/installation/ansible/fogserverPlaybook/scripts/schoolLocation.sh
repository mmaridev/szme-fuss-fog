#!/bin/bash

#### Description: This scripts asks for the school name
#### and keeps it in the variable DLN
#### Copyright 2019 Antonio Nardella - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-2.0
####
#### Version 0.0.1

declare TASK_LIST="schoolName"
declare -A ANSWERS


getinfo()
{
MSG_STR_SCHOOLNAME="Inserire il nome della scuola: (ad esempio Liceo G.Galilei)"
#
#
	# Work the list
	for task in $TASK_LIST
	do
		# Prepare the message string
		str=MSG_STR_${task^^}
		# Loop while input is empty
		while [ -z "${ANSWERS[$task]}" ]
		do
			echo ${!str}
			read -r input
			ANSWERS[$task]=$input
		done
	done
	echo ""
	echo ""
	echo "Nome scuola inserito:"
	for item in $TASK_LIST
	do
		echo "Nome scuola: ${ANSWERS[$item]}"
	done
}


saveschoolname()
{
	echo "{\"schoolName\":\"${ANSWERS[$item]}\"," >> setup.json
	echo "####################################"
	echo "Il nome della scuola è stato salvato"
	echo "####################################"
	echo ""
	echo ""
}

clear
echo "Impostare nome scuola per il Location Plugin del server FOG"
echo ""

getinfo
echo ""
echo "Per piacere verificare il nome inserito!"
echo ""

while true; do
  read -p "Il nome inserito è corretto? [s/n]: " sn
  case $sn in
    [SsYy]* ) saveschoolname; return 0;;
    [Nn]* ) unset ANSWERS; declare -A ANSWERS; getinfo;;
        * ) echo "Per piacere premere s o n!";;
  esac
done
