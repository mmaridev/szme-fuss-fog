FUSS FOG Support
================

Server installation
-------------------

### Install debian 9 (or later)

Debian 9 net-installer

Language English

Location Italy

Keyboard German

#### Partitioning

Manual

Set up LVM Partitions. https://wiki.debian.org/LVM

Be sure to create a specific /image partition to hold the FOG Images as
last partition to make expansion possible

Set mount point to /images

![](media/image1.png)

Enter manually

![](media/image2.png)

/images

![](media/image3.png)

![](media/image4.png)

Choose software to install (Xfce OPTIONAL)![](media/image5.png)

#### Install needed packages

apt update && apt full-upgrade && apt install git net-tools

### Install FOG Server Master Node 1.5.5

Download FOG from
[*https://fogproject.org/download*](https://fogproject.org/download) and
install

or use following script

\#!/bin/sh

\# Antonio Nardella

\# Backup original Truelite dhcp configuration

cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.truelite

\# Download and install FOGProject 1.5.5

wget -O fogproject-1.5.5.tar.gz
<https://github.com/FOGProject/fogproject/archive/1.5.5.tar.gz>

tar -xvzf fogproject-1.5.5.tar.gz

cd fogproject-1.5.5/bin

bash installfog.sh

### Configure FOG

![](media/image6.png)

Debian based installation \[2\]

Normal installation

IP 10.0.10.1

![](media/image7.png)

Change default interface y

DEFAULT LAN interface

Router address: Y

Address of DHCP server: IP

DHCP handle DNS: Y

FOG server for DHCP service N

![](media/image8.png)

Internationalization Y

### Configure DHCP Server for PXE boot

On FUSS Server

nano /etc/dhcp/dhcpd.conf

DELETE group {} block

\# nano /etc/dhcp/dhcpd-added.conf

![](media/image9.png)

group {

next-server \<FOG SERVER IP\>;\
host tftpclient {

filename "undionly.kpxe";

}

}

Restart dhcp service

\# service isc-dhcp-server restart

FOG Security Hardening
----------------------

Follow this guide:

<https://wiki.fogproject.org/wiki/index.php?title=FOG_security>

FUSS Setup

### Proxy user for FOG Server

![](media/image10.png)

Create SSH Keys Share for image automation
------------------------------------------

!!!WARNING EXTREMELY INSECURE!!!

Make a new folder and copy ssh-keys

\# mkdir /etc/fuss-server/ssh-keys\
\# cp /etc/fuss-server/client\_keys/\* /etc/fuss-server/ssh-keys/\
\# chmod -R 444 /etc/fuss-server/ssh-keys/\*

\# nano /etc/exports

\# exportfs --a

FOG Settings

### FOG Scripts for automation

Here are all the snapins scripts used to automate the fuss-client
installation

<https://gitlab.com/antonionardella/szme-fuss-fog>

### Proxy Settings

![](media/image11.png)

### Rebranding

![](media/image12.png)

### Province Bolzano - FOG PXE Boot background

copy szme-fuss-fog/images/pxeboot/bg.png to
/var/www/fog/service/ipxe/bg.png

Fuss Server Upgrade TODO List
-----------------------------

Install FOG Server Storage Node 1.5.5
-------------------------------------

Download FOG from
[*https://fogproject.org/download*](https://fogproject.org/download) and
install

or use following script

\#!/bin/sh

\# Antonio Nardella

\# Download and install FOGProject 1.5.5

wget -O fogproject-1.5.5.tar.gz
<https://github.com/FOGProject/fogproject/archive/1.5.5.tar.gz>

tar -xvzf fogproject-1.5.5.tar.gz

cd fogproject-1.5.5/bin

bash installfog.sh

### Installation FOG

![](media/image6.png)

Debian based installation \[2\]

Normal/Storage Node S

LAN IP 10.0.10.3\
LAN Ethernet Interface eth1

### Configure DHCP Server for PXE boot

\# nano /etc/dhcp/dhcpd.conf

change

filename "pxelinux.0";

![](media/image13.png)

To

filename "undionly.kpxe";

![](media/image14.png)

Restart dhcp service

\# service isc-dhcp-server restart

FOG Security Hardening
----------------------

Follow this guide:

<https://wiki.fogproject.org/wiki/index.php?title=FOG_security>

FUSS Client Installation -- FOG Smart Client installation
---------------------------------------------------------

clone repository
[*https://gitlab.com/antonionardella/*szme-fuss-fog](https://gitlab.com/antonionardella/szme-fuss-fog)

-   sudo apt-get remove \--purge mono-complete
-   sudo rm /etc/apt/sources.list.d/mono-xamarin.list
-   sudo apt-get autoremove
-   sudo apt-get update
-   sudo apt-get install mono-complete
-   Install the client
-   sudo mono SmartInstall.exe

![](media/image15.png)

1.  FOG Server address: IP
2.  Webroot: \<leer lassen\>
3.  Enable tray icon: n
4.  Start FOG Service when done: \<leer lassen\>

FUSS Hacks
----------

crontab -e\
\@reboot
/home/root/szme-fuss-fog/fog-client/FUSS\_client\_9\_boot\_setup.sh

Edit Grub theme

/usr/share/desktop-base/active-theme/grub/grub-4x3.png

Register Clients with FOG
-------------------------

PXE Boot screen

![](media/image16.png)

Follow this guide:

<https://wiki.fogproject.org/wiki/index.php?title=Managing_FOG#Adding_a_new_host>

To do it manually follow this video:

<https://www.youtube.com/watch?v=eoVurK97YsY>
