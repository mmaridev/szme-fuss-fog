#!/bin/bash
#### Filename: installOcsInventoryAgent.sh
#### Creation Date: 2019-04-05T14:55:10+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-04-05T14:55:13+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later

#### ##############################
#### ##          WARNING         ##
#### ##############################
#### 
#### TO USE THIS SCRIPT 
#### 
#### 1. SET THE TAG IN LINE 33
#### 2. IN A FUSS ENVIRONMENT ADD FOLLOWING ACLs TO SQUID
#### 
#### acl repositories url_regex inventory.snets.it
#### acl repositories url_regex github.com
#### acl repositories url_regex s3.amazonaws.com
#### 
#### to /etc/squid3/squid.conf or /etc/squid3/squid-added-repo.conf
#### 
#### and reload the configuration
#### 
#### systemctl reload squid3
####
#### ##############################
#### ##          WARNING         ##
#### ##############################

TAG=<DEFINE ME!!!!>

# Install needed packages
apt  -q -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" install gcc make dmidecode libxml-simple-perl libcompress-zlib-perl libnet-ip-perl libwww-perl libdigest-md5-perl libnet-ssleay-perl libcrypt-ssleay-perl libnet-snmp-perl libproc-pid-file-perl libproc-daemon-perl net-tools libsys-syslog-perl pciutils smartmontools read-edid nmap


# Install OCSInventory agent and ask for TAG

## Download and extract
url=https://github.com/OCSInventory-NG/UnixAgent/releases/download/v2.4.2/Ocsinventory-Unix-Agent-2.4.2.tar.gz
filename=$(basename "$url")
folder=${filename%.tar.gz}

## Installation
rm -rf /etc/ocsinventory*
rm -rf /var/lib/ocsinventory*

cd /tmp
wget "$url"
tar -xvf "$filename"
cd "$folder"
env PERL_AUTOINSTALL=1 perl Makefile.PL && make && make install && perl /tmp/Ocsinventory-Unix-Agent-2.4.2/postinst.pl --nowizard --server=https://inventory.snets.it/ocsinventory --crontab --tag=$TAG --ca=/usr/share/szme-fuss-fog/ocs/server.crt --logfile=/var/log/ocsinventory-agent.log --remove-old-linux-agent --snmp

# Edit configuration file
## Add proxy for FUSS environment 
echo "proxy=https://proxy:8080" >> /etc/ocsinventory-agent/ocsinventory-agent.cfg
echo "AuthRequired=0" >> /etc/ocsinventory-agent/ocsinventory-agent.cfg
echo "SSL=1" >> /etc/ocsinventory-agent/ocsinventory-agent.cfg

# Cleanup
cd /tmp
rm $filename
rm -rf $folder