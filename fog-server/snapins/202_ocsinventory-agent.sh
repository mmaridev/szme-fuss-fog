!/bin/bash
#### Filename: 202_ocsinventory-agent.sh
#### Creation Date: 2019-04-05T14:55:10+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-04-05T14:55:13+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it / Guggi
#### SPDX-License-Identifier: GPL-3.0-or-later

#### ##############################
#### ##          WARNING         ##
#### ##############################
#### 
#### TO USE THIS SCRIPT 
#### 
#### 1. SET THE TAG IN LINE 33
#### 2. IN A FUSS ENVIRONMENT ADD FOLLOWING ACLs TO SQUID
#### 
#### acl repositories url_regex inventory.snets.it
#### acl repositories url_regex github.com
#### acl repositories url_regex s3.amazonaws.com
#### 
#### to /etc/squid3/squid.conf or /etc/squid3/squid-added-repo.conf
#### 
#### and reload the configuration
#### 
#### systemctl reload squid3
####
#### ##############################
#### ##          WARNING         ##
#### ##############################


TAG=$1

# Edit configuration file

cat << EOM > /etc/ocsinventory/ocsinventory-agent.cfg
server=https://inventory.snets.it/ocsinventory
proxy=http://proxy:8080
AuthRequired=0
SSL=1
tag=$TAG
ca=/usr/share/szme-fuss-fog/ocs/server.crt
logfile=/var/log/ocsinventory-agent.log
EOM